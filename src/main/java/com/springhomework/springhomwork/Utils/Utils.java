package com.springhomework.springhomwork.Utils;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

@Component
public class Utils {

    public ModelMapper getMapper(){
        return new ModelMapper();
    }
    public Timestamp getCurrentTime(){
        return new Timestamp(System.currentTimeMillis());
    }
}
