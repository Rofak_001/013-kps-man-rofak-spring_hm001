package com.springhomework.springhomwork.Message;

public class Message {
    public enum Success{
        INSERT_SUCCESS("YOU HAVE INSERTED SUCCESS!"),
        DELETED_SUCCESS("YOU HAVE BEEN DELETED SUCCESS!"),
        UPDATED_SUCCESS("YOU HAVE BEEN UPDATED SUCCESS!"),
        FINDALL_SUCCESS("YOU HAVE FIND SUCCESS!");
        String message;
        Success(String message){
            this.message=message;
        }
        public String getMessage() {
            return message;
        }
    }
    public enum Error{
        DELETED_FAILUR("DELETED FAILED!"),
        UPDATED_FAILED("UPDATED FAILED!"),
        INSERTED_FAILED("INSERTED FAILED!");
        String message;
        Error(String message){
            this.message=message;
        }
        public String getMessage() {
            return message;
        }
    }
}
