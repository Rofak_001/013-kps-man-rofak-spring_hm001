package com.springhomework.springhomwork.Model.Dto;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

public class UsersDto implements UserDetails {
    private int id;
    private String userId;
    private String username;
    private String password;
    private List<RoleDto> roleDtoList;
    public UsersDto(){}
    public UsersDto(String userId, String username, String password,List<RoleDto> roleDtoList) {
        this.userId = userId;
        this.username = username;
        this.password = password;
        this.roleDtoList=roleDtoList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roleDtoList;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List <RoleDto> getRoleDtoList() {
        return roleDtoList;
    }

    public void setRoleDtoList(List <RoleDto> roleDtoList) {
        this.roleDtoList = roleDtoList;
    }

    @Override
    public String toString() {
        return "UsersDto{" +
                "id=" + id +
                ", userId='" + userId + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", roleDtoList=" + roleDtoList +
                '}';
    }
}
