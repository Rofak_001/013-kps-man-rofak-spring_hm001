package com.springhomework.springhomwork.Model.Response;

import com.springhomework.springhomwork.Model.Request.CategoryRequest;

public class BookResponse {
    private String title;
    private String author;
    private String description;
    private String thumbnail;
    private CategoryRespnse category;

    public BookResponse(){}

    public BookResponse(String title, String author, String description, String thumbnail, CategoryRespnse category) {
        this.title = title;
        this.author = author;
        this.description = description;
        this.thumbnail = thumbnail;
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public CategoryRespnse getCategory() {
        return category;
    }

    public void setCategory(CategoryRespnse category) {
        this.category = category;
    }
}
