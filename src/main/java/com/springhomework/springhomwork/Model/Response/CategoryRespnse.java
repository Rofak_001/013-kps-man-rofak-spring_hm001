package com.springhomework.springhomwork.Model.Response;

public class CategoryRespnse {
    private int id;
    private String title;

    public CategoryRespnse(){}

    public CategoryRespnse(int id, String title) {
        this.id = id;
        this.title= title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
