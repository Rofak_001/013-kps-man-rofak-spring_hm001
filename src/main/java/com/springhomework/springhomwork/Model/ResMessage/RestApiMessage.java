package com.springhomework.springhomwork.Model.ResMessage;

import com.springhomework.springhomwork.Pagination.Pagination;
import org.springframework.http.HttpStatus;

import java.sql.Timestamp;

public class RestApiMessage<T> {
    private Pagination pagination;
    private String message;
    private HttpStatus httpStatus;
    private Timestamp timestamp;
    private T data;

    public RestApiMessage(){}


    public RestApiMessage(Pagination pagination,String message, HttpStatus httpStatus, Timestamp timestamp, T data) {
        this.pagination=pagination;
        this.message = message;
        this.httpStatus = httpStatus;
        this.timestamp = timestamp;
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }
}
