package com.springhomework.springhomwork.Model.ResMessage;

import org.springframework.http.HttpStatus;

public class SimpleMassage {
    private String message;
    private HttpStatus httpStatus;

    public SimpleMassage(){}
    public SimpleMassage(String message, HttpStatus httpStatus) {
        this.message = message;
        this.httpStatus = httpStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }
}
