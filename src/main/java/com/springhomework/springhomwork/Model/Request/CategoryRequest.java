package com.springhomework.springhomwork.Model.Request;

public class CategoryRequest {
    private int id;
    private String title;
    public CategoryRequest(){}

    public CategoryRequest( String title) {

        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
