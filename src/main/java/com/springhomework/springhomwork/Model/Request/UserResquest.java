package com.springhomework.springhomwork.Model.Request;

import com.springhomework.springhomwork.Model.Dto.RoleDto;

import java.util.List;

public class UserResquest {
    private String username;
    private String password;
    private List<RoleDto> roleDtoList;
    public UserResquest(){}
    public UserResquest(String username, String password , List<RoleDto> roleDtoList) {
        this.username = username;
        this.password = password;
        this.roleDtoList=roleDtoList;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List <RoleDto> getRoleDtoList() {
        return roleDtoList;
    }

    public void setRoleDtoList(List <RoleDto> roleDtoList) {
        this.roleDtoList = roleDtoList;
    }
}
