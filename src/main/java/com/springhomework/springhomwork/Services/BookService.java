package com.springhomework.springhomwork.Services;


import com.springhomework.springhomwork.Model.Dto.BookDto;
import com.springhomework.springhomwork.Pagination.Pagination;
import com.springhomework.springhomwork.Repository.Provider.BookProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;

public interface BookService {

    BookDto insertBook(BookDto bookDto);

    String deleteBook(int id);

    BookDto updateBook(int id, BookDto bookDto);

    List <BookDto> findAll(@Param("pagination") Pagination pagination);

    BookDto findOneById(int id);

    int countAllBook();

    List <BookDto> filterByTitle(@Param("pagination") Pagination pagination, @Param("title") String string);

    int coutByFilterTitle(@Param("title") String title);

    List <BookDto> searchCatById(@Param("pagination") Pagination pagination, @Param("category_id") int category_id);

    int countBookByCategoryId(@Param("category_id") int category_id);

    List <BookDto> searchByCatIdAndTitle(@Param("title") String title, @Param("category_id") int category_id, @Param("pagination") Pagination pagination);

    int countBookByCatAndTitle(@Param("title") String title, @Param("category_id") int category_id);
}
