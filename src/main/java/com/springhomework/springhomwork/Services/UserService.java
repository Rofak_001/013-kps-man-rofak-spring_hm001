package com.springhomework.springhomwork.Services;

import com.springhomework.springhomwork.Model.Dto.UsersDto;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {
    UsersDto insertUser(UsersDto usersDto);
}
