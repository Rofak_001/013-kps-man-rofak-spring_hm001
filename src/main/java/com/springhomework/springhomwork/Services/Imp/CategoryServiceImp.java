package com.springhomework.springhomwork.Services.Imp;

import com.springhomework.springhomwork.Message.Message;
import com.springhomework.springhomwork.Model.Dto.CategoryDto;
import com.springhomework.springhomwork.Pagination.Pagination;
import com.springhomework.springhomwork.Repository.CategoryRepository;
import com.springhomework.springhomwork.Services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImp implements CategoryService {
    private CategoryRepository repository;

    @Autowired
    public void setRepository(CategoryRepository repository) {
        this.repository = repository;
    }

    @Override
    public String insertCategory(CategoryDto categoryDto) {
        boolean isInserted =repository.insertCategory(categoryDto);
        if(isInserted){
            return Message.Success.INSERT_SUCCESS.getMessage();
        }
        return Message.Error.INSERTED_FAILED.getMessage();
    }

    @Override
    public String deleteCategory(int id) {
        boolean isDelete=repository.deleteCategory(id);
        if(isDelete){
            return Message.Success.DELETED_SUCCESS.getMessage();
        }else {
            return Message.Error.DELETED_FAILUR.getMessage();
        }
    }

    @Override
    public boolean updateCategory(int id, CategoryDto categoryDto) {
        boolean isUpdated=repository.updateCategory(id,categoryDto);
        if(isUpdated){
          return true;
        }else {
            return false;
        }
    }

    @Override
    public List<CategoryDto> findAll(Pagination pagination) {
        return repository.findAll(pagination);
    }

    @Override
    public int countAllCategory() {
        return  repository.countAllCategory();
    }


}
