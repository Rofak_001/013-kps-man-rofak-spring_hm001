package com.springhomework.springhomwork.Services.Imp;

import com.springhomework.springhomwork.Message.Message;
import com.springhomework.springhomwork.Model.Dto.BookDto;
import com.springhomework.springhomwork.Pagination.Pagination;
import com.springhomework.springhomwork.Repository.BookRepository;
import com.springhomework.springhomwork.Services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImp implements BookService {
    private BookRepository bookRepository;

    @Autowired
    public void setBookRepository(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public BookDto insertBook(BookDto bookDto) {
        boolean isInserted=bookRepository.insertBook(bookDto);
        if(isInserted){
            String title=bookRepository.selectCategoryTitleById(bookDto.getCategory().getId());
            bookDto.getCategory().setTitle(title);
            return bookDto;
        }else {
            return null;
        }
    }

    @Override
    public String deleteBook(int id) {
        boolean isDeleted=bookRepository.deleteBook(id);
        if(isDeleted){
            return Message.Success.DELETED_SUCCESS.getMessage();
        }else {
            return Message.Error.DELETED_FAILUR.getMessage();
        }
    }

    @Override
    public BookDto updateBook(int id, BookDto bookDto) {
        boolean isUpdate=bookRepository.updateBook(id,bookDto);
        if(isUpdate){
            String title=bookRepository.selectCategoryTitleById(bookDto.getCategory().getId());
            bookDto.getCategory().setTitle(title);
            return bookDto;
        }else {
            return null;
        }
    }

    @Override
    public List<BookDto> findAll(Pagination pagination) {
        return bookRepository.findAll(pagination);
    }

    @Override
    public BookDto findOneById(int id) {
        BookDto bookDto=bookRepository.findOneById(id);
        if(bookDto!=null){
            return bookDto;
        }else {
            return null;
        }
    }

    @Override
    public int countAllBook() {
        return  bookRepository.countAllBook();
    }

    @Override
    public List <BookDto> filterByTitle(Pagination pagination,String title) {
        return bookRepository.filterByTitle(pagination,title);
    }

    @Override
    public int coutByFilterTitle(String title) {
        System.out.println("Coumt"+bookRepository.coutByFilterTitle(title));
        return bookRepository.coutByFilterTitle(title);
    }

    @Override
    public List <BookDto> searchCatById(Pagination pagination, int category_id) {
        return bookRepository.searchCatById(pagination,category_id);
    }

    @Override
    public int countBookByCategoryId(int category_id) {
        System.out.println("Count Book By:"+bookRepository.countBookByCategoryId(category_id));
        return bookRepository.countBookByCategoryId(category_id);
    }

    @Override
    public List <BookDto> searchByCatIdAndTitle(String title, int category_id, Pagination pagination) {
        return bookRepository.searchByCatIdAndTitle(title,category_id,pagination);
    }

    @Override
    public int countBookByCatAndTitle(String title, int category_id) {
        return bookRepository.countBookByCatAndTitle(title,category_id);
    }
}
