package com.springhomework.springhomwork.Services.Imp;

import com.springhomework.springhomwork.Model.Dto.RoleDto;
import com.springhomework.springhomwork.Model.Dto.UsersDto;
import com.springhomework.springhomwork.Repository.UserRepository;
import com.springhomework.springhomwork.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class UserServiceImp implements UserService {
    private UserRepository userRepository;

    @Autowired
    public UserServiceImp(UserRepository userRepository){
        this.userRepository=userRepository;
    }
    @Override
    public UsersDto insertUser(UsersDto usersDto) {
        try {
            boolean isInserted=userRepository.insert(usersDto);
            if(isInserted){
                int id=userRepository.selectUserById(usersDto.getUserId());
                usersDto.setId(id);
                for(RoleDto role:usersDto.getRoleDtoList()){
                    userRepository.createUserRole(usersDto,role);
                }
            }
            return isInserted?usersDto:null;
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_GATEWAY,e.getMessage());
        }
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        UsersDto usersDto=userRepository.selectUserByUsername(s);
        System.out.println(usersDto);
        System.out.println(userRepository.selectRolesByUserId(usersDto.getId()));
        return usersDto;
    }
}
