package com.springhomework.springhomwork.Services;

import com.springhomework.springhomwork.Model.Dto.CategoryDto;
import com.springhomework.springhomwork.Pagination.Pagination;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CategoryService {
    String insertCategory(CategoryDto categoryDto);
    String deleteCategory(int id);
    boolean updateCategory(int id,CategoryDto categoryDto);
    List<CategoryDto> findAll(@Param("pagination")Pagination pagination);
    int countAllCategory();
}
