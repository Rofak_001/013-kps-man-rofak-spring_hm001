package com.springhomework.springhomwork.Repository;

import com.springhomework.springhomwork.Model.Dto.BookDto;
import com.springhomework.springhomwork.Model.Dto.CategoryDto;
import com.springhomework.springhomwork.Model.Response.BookResponse;
import com.springhomework.springhomwork.Pagination.Pagination;
import com.springhomework.springhomwork.Repository.Provider.BookProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository {
    @InsertProvider(value = BookProvider.class, method = "insertBook")
    boolean insertBook(BookDto bookDto);

    @DeleteProvider(value = BookProvider.class, method = "deleteBook")
    boolean deleteBook(int id);

    @SelectProvider(value = BookProvider.class, method = "selectCategoryTitleById")
    String selectCategoryTitleById(int id);

    @UpdateProvider(value = BookProvider.class, method = "updateBook")
    boolean updateBook(int id, BookDto bookDto);

    @SelectProvider(value = BookProvider.class, method = "findAll")
    @Results({
            @Result(column = "category_id", property = "category", many = @Many(select = "selectCatById")),

    })
    List <BookDto> findAll(@Param("pagination") Pagination pagination);

    @SelectProvider(value = BookProvider.class, method = "selectCatById")
    CategoryDto selectCatById(int category_id);

    @SelectProvider(value = BookProvider.class, method = "findOneById")
    @Results({
            @Result(column = "category_id", property = "category", many = @Many(select = "selectCatById")),

    })
    BookDto findOneById(int id);

    @SelectProvider(value = BookProvider.class, method = "countAllBook")
    int countAllBook();

    @SelectProvider(value = BookProvider.class, method = "filterByTitle")
    @Results({
            @Result(column = "category_id", property = "category", many = @Many(select = "selectCatById")),

    })
    List <BookDto> filterByTitle(@Param("pagination") Pagination pagination, @Param("title") String string);

    @SelectProvider(value = BookProvider.class, method = "coutByFilterTitle")
    int coutByFilterTitle(@Param("title") String title);

    @SelectProvider(value = BookProvider.class, method = "searchCatById")
    @Results({
            @Result(column = "category_id", property = "category", many = @Many(select = "selectCatById")),
    })
    List <BookDto> searchCatById(@Param("pagination") Pagination pagination, @Param("category_id") int category_id);

    @SelectProvider(value = BookProvider.class, method = "countBookByCategoryId")
    int countBookByCategoryId(@Param("category_id") int category_id);

    @SelectProvider(value = BookProvider.class,method = "searchByCatIdAndTitle")
    @Results({
            @Result(column = "category_id", property = "category", many = @Many(select = "selectCatById")),
    })
    List<BookDto> searchByCatIdAndTitle(@Param("title")String title,@Param("category_id") int category_id,@Param("pagination") Pagination pagination);

    @SelectProvider(value = BookProvider.class,method = "countBookByCatAndTitle")
    int  countBookByCatAndTitle(@Param("title")String title,@Param("category_id") int category_id);
}
