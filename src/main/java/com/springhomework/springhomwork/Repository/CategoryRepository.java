package com.springhomework.springhomwork.Repository;

import com.springhomework.springhomwork.Model.Dto.CategoryDto;
import com.springhomework.springhomwork.Pagination.Pagination;
import com.springhomework.springhomwork.Repository.Provider.CategoryProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository {

    @InsertProvider(value = CategoryProvider.class,method = "insertCategory")
    boolean insertCategory(CategoryDto categoryDto);

    @DeleteProvider(value =CategoryProvider.class,method = "delete")
    boolean deleteCategory(int id);

    @UpdateProvider(value = CategoryProvider.class,method = "Update")
    boolean updateCategory(int id,CategoryDto categoryDto);

    @SelectProvider(value = CategoryProvider.class,method = "findAll")
    List<CategoryDto> findAll(@Param("pagination")Pagination pagination);

    @SelectProvider(value = CategoryProvider.class,method = "countAllCategory")
    int countAllCategory();


}
