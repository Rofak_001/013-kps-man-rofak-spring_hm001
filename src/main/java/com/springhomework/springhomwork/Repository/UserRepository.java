package com.springhomework.springhomwork.Repository;

import com.springhomework.springhomwork.Model.Dto.RoleDto;
import com.springhomework.springhomwork.Model.Dto.UsersDto;
import com.springhomework.springhomwork.Repository.Provider.UserProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Repository
public interface UserRepository {
//    @InsertProvider(value = UserProvider.class,method = "insertUser")
//    @Results({
//            @Result(column = "role_id",property = "roleDto",many = @Many(select = "selectRoleById"))
//    })
//    boolean insetUser(UsersDto userDto);
//    @SelectProvider(value = UserProvider.class,method = "selectRoleById")
//    List<RoleDto>selectRoleById();
//    @SelectProvider(value = UserProvider.class,method = "selectUserByUsername")
//    UsersDto selectUserByUsername(String username);
@InsertProvider(value = UserProvider.class, method = "insertUserSQL")
boolean insert(UsersDto usersDto) throws ResponseStatusException;

    @InsertProvider(value = UserProvider.class, method = "createUserRole")
    boolean createUserRole(UsersDto usersDto, RoleDto roleDto);

    @SelectProvider(value = UserProvider.class, method = "selectUserById")
    int selectUserById(String userId);

    @SelectProvider(value = UserProvider.class, method = "selectUserByUsername")
    @Results({
            @Result(column = "user_id",property = "userId"),
            @Result(column = "id",
                    property = "roleDtoList",
                    many = @Many(select = "selectRolesByUserId"))
    }
    )
    UsersDto selectUserByUsername(String username);

    @SelectProvider(value = UserProvider.class,method = "selectRolesByUserIdSql")
    List<RoleDto> selectRolesByUserId(int id);
}
