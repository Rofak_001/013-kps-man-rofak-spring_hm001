package com.springhomework.springhomwork.Repository.Provider;


import org.apache.ibatis.jdbc.SQL;

public class BookProvider {
    public String insertBook(){
        return new SQL(){{
            INSERT_INTO("tb_books");
            VALUES("title","#{title}");
            VALUES("category_id","#{category.id}");
            VALUES("author","#{author}");
            VALUES("description","#{description}");
            VALUES("thumbnail","#{thumbnail}");
        }}.toString();
    }
    public String deleteBook(){
        return new SQL(){{
            DELETE_FROM("tb_books");
            WHERE("id=#{id}");
        }}.toString();
    }
    public String selectCategoryTitleById(){
        return new SQL(){{
            SELECT("title");
            FROM("tb_categories");
            WHERE("id=#{id}");

        }}.toString();
    }
    public String updateBook(){
        return new SQL(){{
            UPDATE("tb_books");
            SET("title=#{arg1.title}");
            SET("category_id=#{arg1.category.id}");
            SET("author=#{arg1.author}");
            SET("description=#{arg1.description}");
            SET("thumbnail=#{arg1.thumbnail}");
            WHERE("id=#{arg0}");
        }}.toString();
    }

    public String findAll(){
        return new SQL(){{
            SELECT("*");
            FROM("tb_books");
            ORDER_BY("id");
            LIMIT("#{pagination.limit}");
            OFFSET("#{pagination.offset}");
        }}.toString();
    }
    public String selectCatById(){
        return new  SQL(){{
            SELECT("*");
            FROM("tb_categories");
            WHERE("id=#{category_id}");
        }}.toString();
    }
    public String findOneById(){
        return new SQL(){{
            SELECT("*");
            FROM("tb_books");
            WHERE("id=#{id}");
        }}.toString();
    }

    public String countAllBook(){
        return new SQL(){{
            SELECT("COUNT(id)");
            FROM("tb_books");
        }}.toString();
    }
    //search by title
    public String filterByTitle(){
        return new SQL(){{
            SELECT("*");
            FROM("tb_books");
            ORDER_BY("id");
            LIMIT("#{pagination.limit}");
            OFFSET("#{pagination.offset}");
            WHERE("title LIKE '%'||#{title}||'%'");
        }}.toString();
    }
    public String coutByFilterTitle(){
        return new SQL(){{
            SELECT("COUNT(id)");
            FROM("tb_books");
            WHERE("title LIKE '%'||#{title}||'%'");
        }}.toString();
    }
    // Search by Cat
    public String searchCatById(){
        return new SQL(){{
            SELECT("*");
            FROM("tb_books");
            LIMIT("#{pagination.limit}");
            OFFSET("#{pagination.offset}");
            WHERE("category_id=#{category_id}");
        }}.toString();
    }
    public String countBookByCategoryId(){
        return new SQL(){{
            SELECT("COUNT(id)");
            FROM("tb_books");
            WHERE("category_id=#{category_id}");
        }}.toString();
    }

    //Search by catId and Tilte
    public String searchByCatIdAndTitle(){
        return new SQL(){{
            SELECT("*");
            FROM("tb_books");
            LIMIT("#{pagination.limit}");
            OFFSET("#{pagination.offset}");
            WHERE("category_id=#{category_id} AND title LIKE '%'||#{title}||'%'");
        }}.toString();
    }
    public String countBookByCatAndTitle(){
        return new SQL(){{
            SELECT("COUNT(id)");
            FROM("tb_books");
            WHERE("category_id=#{category_id} AND title LIKE '%'||#{title}||'%'");
        }}.toString();
    }
}
