package com.springhomework.springhomwork.Repository.Provider;


import org.apache.ibatis.jdbc.SQL;

public class CategoryProvider {
    public String insertCategory(){
        return new SQL(){{
            INSERT_INTO("tb_categories");
            VALUES("title","#{title}");
        }}.toString();
    }

    public String delete(){
        return new SQL(){{
            DELETE_FROM("tb_categories");
            WHERE("id=#{id}");
        }}.toString();
    }

    public String Update(){
        return new SQL(){{
            UPDATE("tb_categories");
            SET("title=#{arg1.title}");
            WHERE("id=#{arg0}");
        }}.toString();
    }
    public String findAll(){
        return new SQL(){{
            SELECT("*");
            FROM("tb_categories");
            ORDER_BY("id");
            LIMIT("#{pagination.limit}");
            OFFSET("#{pagination.offset}");
        }}.toString();
    }

    public String countAllCategory(){
        return new SQL(){{
            SELECT("COUNT(id)");
            FROM("tb_categories");
        }}.toString();
    }

}
