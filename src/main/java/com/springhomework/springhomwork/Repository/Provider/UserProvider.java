package com.springhomework.springhomwork.Repository.Provider;

import org.apache.ibatis.jdbc.SQL;

public class UserProvider {

//    public String insertUser(){
//        return new SQL(){{
//            INSERT_INTO("tb_users");
//            VALUES("role_id","#{role_id}");
//            VALUES("username","#{username}");
//            VALUES("password","#{password}");
//        }}.toString();
//    }
//
//    public String selectUserByUsername(){
//        return new SQL(){{
//            SELECT("*");
//            FROM("tb_users");
//            WHERE("username=#{username}");
//        }}.toString();
//    }
//    public String selectRoleById(){
//        return new SQL(){{
//            SELECT("*");
//            FROM("tb_role");
//            WHERE("id=#{role_id}");
//        }}.toString();
//    }


    public String insertUserSQL(){
        return new SQL(){{
            INSERT_INTO("users");
            VALUES("user_id","#{userId}");
            VALUES("username","#{username}");
            VALUES("password","#{password}");
        }}.toString();
    }
    public String createUserRole(){
        return new SQL(){{
            INSERT_INTO("users_roles");
            VALUES("user_id","#{arg0.id}");
            VALUES("role_id","#{arg1.id}");
        }}.toString();
    }
    public String selectUserById(){
        return new SQL(){{
            SELECT("id");
            FROM("users");
            WHERE("user_id=#{userId}");

        }}.toString();
    }
    public String selectUserByUsername(){
        return new SQL(){{
            SELECT("*");
            FROM("users");
            WHERE("username=#{username}");
        }}.toString();
    }
    public String selectRolesByUserIdSql(int Id) {
        return new SQL(){{
            SELECT("r.id, r.name");
            FROM("roles r");
            INNER_JOIN("users_roles ur ON r.id = ur.role_id");
            WHERE("ur.user_id = '" + Id +"'");
        }}.toString();
    }
}
