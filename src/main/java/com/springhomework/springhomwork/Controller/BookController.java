package com.springhomework.springhomwork.Controller;


import com.springhomework.springhomwork.Message.Message;
import com.springhomework.springhomwork.Model.Dto.BookDto;
import com.springhomework.springhomwork.Model.Request.BookRequest;
import com.springhomework.springhomwork.Model.ResMessage.RestApiMessage;
import com.springhomework.springhomwork.Model.ResMessage.SimpleMassage;
import com.springhomework.springhomwork.Model.Response.BookResponse;
import com.springhomework.springhomwork.Pagination.Pagination;
import com.springhomework.springhomwork.Services.BookService;
import com.springhomework.springhomwork.Utils.Utils;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.List;

@RestController
@RequestMapping("api/v1")
@Api(tags = "Books")
public class BookController {
    private BookService bookService;
    private Utils utils;

    @Autowired
    public void setUtils(Utils utils) {
        this.utils = utils;
    }

    @Autowired
    public void setBookService(BookService bookService) {
        this.bookService = bookService;
    }

    @PostMapping("/books")
    public ResponseEntity <RestApiMessage <BookResponse>> InsertBook(@RequestBody BookRequest bookRequest) {
        BookDto bookDto = utils.getMapper().map(bookRequest, BookDto.class);
        BookResponse bookResponse = utils.getMapper().map(bookService.insertBook(bookDto), BookResponse.class);
        RestApiMessage <BookResponse> responseMessage = new RestApiMessage <>();
        responseMessage.setData(bookResponse);
        responseMessage.setMessage(Message.Success.INSERT_SUCCESS.getMessage());
        responseMessage.setHttpStatus(HttpStatus.CREATED);
        responseMessage.setTimestamp(utils.getCurrentTime());
        return ResponseEntity.ok(responseMessage);
    }
    @DeleteMapping("/books/{id}")
    public ResponseEntity <SimpleMassage> deleteBook(@PathVariable int id) {
        SimpleMassage responseMessage = new SimpleMassage();
        String msg = bookService.deleteBook(id);
        responseMessage.setMessage(msg);
        responseMessage.setHttpStatus(HttpStatus.OK);
        return new ResponseEntity <>(responseMessage, HttpStatus.OK);
    }

    @PutMapping("/books/{id}")
    public ResponseEntity <RestApiMessage <BookResponse>> updateBook(@PathVariable int id, @RequestBody BookRequest bookRequest) {
        BookDto bookDto = utils.getMapper().map(bookRequest, BookDto.class);
        BookResponse bookResponse = utils.getMapper().map(bookService.updateBook(id, bookDto), BookResponse.class);
        RestApiMessage <BookResponse> responseMessage = new RestApiMessage <>();
        responseMessage.setData(bookResponse);
        responseMessage.setMessage(Message.Success.UPDATED_SUCCESS.getMessage());
        responseMessage.setHttpStatus(HttpStatus.OK);
        responseMessage.setTimestamp(utils.getCurrentTime());
        return ResponseEntity.ok(responseMessage);
    }
    @GetMapping(value = "/books")
    public ResponseEntity <RestApiMessage <List <BookDto>>> findAll(
            @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @RequestParam(value = "limit", required = false, defaultValue = "5") int limit,
            @RequestParam(value = "title",required = false) String title,
            @RequestParam(value = "category_id",required = false ,defaultValue = "0")int category_id
    ) {
        //pagination
        Pagination pagination = new Pagination(page, limit);
        RestApiMessage <List <BookDto>> restApiMessage = new RestApiMessage <>();
        pagination.setPage(page);
        pagination.setLimit(limit);
        if(title!=null && category_id!=0){
            pagination.setTotalCount(bookService.countBookByCatAndTitle(title,category_id));
            pagination.setTotalPages(pagination.getTotalPages());
            System.out.println("Count Book by Title and Category:"+bookService.countBookByCatAndTitle(title,category_id));
            restApiMessage.setData(bookService.searchByCatIdAndTitle(title,category_id,pagination));
        } else if(title!=null){
            pagination.setTotalCount(bookService.coutByFilterTitle(title));
            pagination.setTotalPages(pagination.getTotalPages());
            System.out.println("Count Book by Title"+bookService.coutByFilterTitle(title));
            restApiMessage.setData(bookService.filterByTitle(pagination,title));
        }else if(category_id!=0){
            System.out.println("Hhhe");
            pagination.setTotalCount(bookService.countBookByCategoryId(category_id));
            pagination.setTotalPages(pagination.getTotalPages());
            System.out.println("Count Book By category:"+bookService.countBookByCategoryId(category_id));
            restApiMessage.setData(bookService.searchCatById(pagination,category_id));
        } else {
            pagination.setTotalCount(bookService.countAllBook());
            System.out.println("Count All Book:"+bookService.countAllBook());
            pagination.setTotalPages(pagination.getTotalPages());
            restApiMessage.setData(bookService.findAll(pagination));
        }

        restApiMessage.setPagination(pagination);
        restApiMessage.setHttpStatus(HttpStatus.FOUND);
        restApiMessage.setTimestamp(utils.getCurrentTime());
        restApiMessage.setMessage(Message.Success.FINDALL_SUCCESS.getMessage());
        return ResponseEntity.ok(restApiMessage);
    }



    @GetMapping("/books/{id}")
    public ResponseEntity <RestApiMessage <BookResponse>> findOneById(@PathVariable int id) {
        BookResponse bookResponse = utils.getMapper().map(bookService.findOneById(id), BookResponse.class);
        RestApiMessage <BookResponse> restApiMessage = new RestApiMessage <>();
        restApiMessage.setMessage(Message.Success.FINDALL_SUCCESS.getMessage());
        restApiMessage.setTimestamp(utils.getCurrentTime());
        restApiMessage.setHttpStatus(HttpStatus.FOUND);
        restApiMessage.setData(bookResponse);
        return ResponseEntity.ok(restApiMessage);
    }

}
