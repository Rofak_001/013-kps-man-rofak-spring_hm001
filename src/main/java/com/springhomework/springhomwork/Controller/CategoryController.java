package com.springhomework.springhomwork.Controller;

import com.springhomework.springhomwork.Message.Message;
import com.springhomework.springhomwork.Model.Dto.CategoryDto;
import com.springhomework.springhomwork.Model.Request.CategoryRequest;
import com.springhomework.springhomwork.Model.ResMessage.RestApiMessage;
import com.springhomework.springhomwork.Model.ResMessage.SimpleMassage;
import com.springhomework.springhomwork.Model.Response.CategoryRespnse;
import com.springhomework.springhomwork.Pagination.Pagination;
import com.springhomework.springhomwork.Services.CategoryService;
import com.springhomework.springhomwork.Utils.Utils;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1")
@Api(tags = "Category")
public class CategoryController {
    private Utils utils;
    private CategoryService categoryService;

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @Autowired
    public void setUtils(Utils utils) {
        this.utils = utils;
    }

    @PostMapping("/category")
    public ResponseEntity <SimpleMassage> insertCategory(@RequestBody CategoryRequest categoryRequest) {
        CategoryDto categoryDto = utils.getMapper().map(categoryRequest, CategoryDto.class);
        SimpleMassage simpleMassage = new SimpleMassage();
        String inserted = categoryService.insertCategory(categoryDto);
        simpleMassage.setMessage(inserted);
        simpleMassage.setHttpStatus(HttpStatus.CREATED);
        return ResponseEntity.ok(simpleMassage);
    }

    @DeleteMapping("/category/{id}")
    public ResponseEntity <SimpleMassage> deleteCateogry(@PathVariable int id) {
        SimpleMassage simpleMassage = new SimpleMassage();
        String msg = categoryService.deleteCategory(id);
        simpleMassage.setMessage(msg);
        if (msg.equalsIgnoreCase(Message.Success.DELETED_SUCCESS.getMessage())) {
            simpleMassage.setHttpStatus(HttpStatus.OK);
        } else {
            simpleMassage.setHttpStatus(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(simpleMassage);
    }

    @PutMapping("/category/{id}")
    public ResponseEntity <SimpleMassage> updateCategory(@PathVariable int id, @RequestBody CategoryRequest categoryRequest) {
        SimpleMassage simpleMassage = new SimpleMassage();
        CategoryDto categoryDto = utils.getMapper().map(categoryRequest, CategoryDto.class);
        boolean isUpated = categoryService.updateCategory(id, categoryDto);
        if (isUpated) {
            simpleMassage.setMessage(Message.Success.UPDATED_SUCCESS.getMessage());
            simpleMassage.setHttpStatus(HttpStatus.OK);
        } else {
            simpleMassage.setMessage(Message.Error.UPDATED_FAILED.getMessage());
            simpleMassage.setHttpStatus(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(simpleMassage);
    }
    @GetMapping("/category")
    public ResponseEntity<RestApiMessage<List<CategoryDto>>> findAllCategory(
            @RequestParam (value = "page",required = false,defaultValue = "1")int page,
            @RequestParam (value = "limit",required = false,defaultValue = "3")int limit
            ){
        Pagination pagination=new Pagination(page,limit);
        pagination.setPage(page);
        pagination.setLimit(limit);
        pagination.setTotalCount(categoryService.countAllCategory());
        pagination.setTotalPages(pagination.getTotalPages());
        RestApiMessage restApiMessage=new RestApiMessage();
        restApiMessage.setPagination(pagination);
        restApiMessage.setData(categoryService.findAll(pagination));
        restApiMessage.setMessage(Message.Success.FINDALL_SUCCESS.getMessage());
        restApiMessage.setTimestamp(utils.getCurrentTime());
        restApiMessage.setHttpStatus(HttpStatus.FOUND);
        return ResponseEntity.ok(restApiMessage);
    }

}
