package com.springhomework.springhomwork.Controller;

import com.springhomework.springhomwork.Message.Message;
import com.springhomework.springhomwork.Model.Dto.UsersDto;
import com.springhomework.springhomwork.Model.Request.UserResquest;
import com.springhomework.springhomwork.Model.ResMessage.RestApiMessage;
import com.springhomework.springhomwork.Model.Response.UserRest;
import com.springhomework.springhomwork.Services.UserService;
import com.springhomework.springhomwork.Utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;
import java.util.UUID;

@RestController
@RequestMapping("api/v1")
public class UserController {
    private UserService userService;
    private Utils utils;
    private BCryptPasswordEncoder encoder;

    @Autowired
    public void setEncoder(BCryptPasswordEncoder encoder) {
        this.encoder = encoder;
    }

    @Autowired
    public void setUtils(Utils utils) {
        this.utils = utils;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/users")
    public ResponseEntity <RestApiMessage<UserRest>> insertUser(@RequestBody UserResquest userResquest) {
        UsersDto dto = utils.getMapper().map(userResquest, UsersDto.class);

        dto.setUserId(UUID.randomUUID().toString());
        dto.setPassword(encoder.encode(dto.getPassword()));
        UserRest rest = utils.getMapper().map(userService.insertUser(dto), UserRest.class);
        RestApiMessage <UserRest> response = new RestApiMessage <>();
        response.setMessage(Message.Success.INSERT_SUCCESS.getMessage());
        response.setData(rest);
        response.setHttpStatus(HttpStatus.ACCEPTED);
        response.setTimestamp(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }
}
