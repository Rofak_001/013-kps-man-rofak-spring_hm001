package com.springhomework.springhomwork.Configuration;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionConfiguration {
    @ExceptionHandler(Exception.class)
    public ResponseEntity<String > handle(Exception e){
        return new ResponseEntity <>(e.getMessage(),HttpStatus.BAD_GATEWAY);
    }
}
